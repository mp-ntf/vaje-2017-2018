#!/usr/bin/env python3
from ipdb import set_trace

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import argparse
import sys
import os
import random


# Program argumenst
parser = argparse.ArgumentParser(
    description="""Program za izris mikrostrukture""",
    usage='use "%(prog)s --help" for more information',
    )

parser.add_argument(
    "--verbose", "-v",
    action="count",
    default=0,
    help="Podrobnejsi izpis delovanja programa, vec -v da se bolj podroben izpis",
    )

# End of options
parser.add_argument(
    "data_sources",
    type=str,
    nargs='+',
    help="""Datoteke z mikrostrukturo za izris""",
    )

def f_convert( i, N ) :
    if i == 0 :
        return 0
    if i == -1 :
        return 1
    else :
        return i%N + 2

def plot( Z, fn_out, ngrains ) :

    grains = np.unique(Z)

    xmin, xmax = 0, Z.shape[0]+1
    ymin, ymax = 0, Z.shape[1]+1
    plot_norm = None
    extent = ( xmin, xmax, ymin, ymax )

    N = 256
    # define the colormap
    # cmap = plt.cm.jet
    cmap = plt.cm.gist_ncar
    # cmap = plt.cm.tab20
    # extract all colors from the .jet map
    # cmaplist = [cmap(i) for i in range(0,cmap.N,5)]
    cmaplist = [cmap(i) for i in range(cmap.N)]
    random.seed(123456)
    random.shuffle(cmaplist)
    cmaplist = [ ( 1., 1., 1., 1.), (0., 0., 0., 1.), ] + cmaplist
    # create the new map
    Z_new = np.vectorize( f_convert )( Z, N )
    cmap = cmap.from_list('Custom cmap', cmaplist, len(cmaplist) )

    # define the bins and normalize
    bounds = np.linspace(0-0.5,len(cmaplist)-0.5,len(cmaplist)+1)
    norm = mpl.colors.BoundaryNorm(bounds, len(cmaplist) )

    # set_trace()
    plt.imshow( Z_new, interpolation='none', extent=extent, origin="lower",
            cmap=cmap, norm=norm )
    # plt.imshow( Z, interpolation='none', vmin=vmin, vmax=vmax, extent=extent, origin="lower", norm=plot_norm )
    plt.xlabel("X")
    plt.ylabel("Y")

    # plt.colorbar()
    plt.xlim( xmin=xmin, xmax=xmax )
    plt.ylim( ymin=ymin, ymax=ymax )
    # z_avg, z_min, z_max = np.average(Z), np.min(Z), np.max(Z)
    plt.title( "%s\nStevilo zrn = %d" % (fn_out,ngrains,) )
    plt.savefig( fn_out, dpi=150, format="png" )
    plt.clf()

if __name__ == "__main__" :
    args = parser.parse_args()

    import logging
    logger = logging.getLogger()

    if args.verbose >= 2 :
        logger.setLevel(logging.DEBUG)
    else :
        logger.setLevel( [logging.ERROR, logging.INFO, ][args.verbose] )

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(levelname)6s| %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    for src in args.data_sources :
        dst = src.replace(".txt","") + ".png"
        logger.info("Izrisujem podatke v datoteki \"%s\" v datoteko \"%s\""
            % ( src, dst ) )
        try :
            x, y, z = np.loadtxt( src, unpack=True, dtype=np.int32 )
            M, N = np.max(x)+1, np.max(y)+1
            Z = np.empty( (M, N, ), dtype=np.int32 )
            Z[:] = -1
            for xx, yy, zz in zip(x,y,z) :
                Z[xx,yy] = zz
            ZZ = np.empty( (M+2, N+2, ), dtype=np.int32 )
            ZZ[:] = -1
            ZZ[1:-1,1:-1] = Z
            ZZ[0,:] = ZZ[-2,:]
            ZZ[-1,:] = ZZ[1,:]
            ZZ[:,0] = ZZ[:,-2]
            ZZ[:,-1] = ZZ[:,1]
            for i in range(1,M+1) :
                for j in range(1,N+1) :
                    if [ ZZ[i,j], ] * 4 != [ ZZ[i+1,j], ZZ[i,j+1],
                            ZZ[i-1,j], ZZ[i,j-1], ] :
                        Z[i-1,j-1] = -1
            plot( Z, dst, sum( np.unique(z) >= 1 ) )
        except FileNotFoundError :
            logger.error( "Datoteka \"%s\" ne obstaja, izpuscam jo in nadaljujem" % (src,) )






