#include "CA2Dzgradba.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CA2Dzgradba::CA2Dzgradba(int tdx, int tdy)
{
	Nx = tdx;
	Ny = tdy;

	Nnx = Nx+2;
	Nny = Ny+2;
	
	int velikostv = Nnx*Nny;
	t0 = new Celica[velikostv];
	t1 = new Celica[velikostv];
	random = new Rand;

	IniciacijaCelic();
}

CA2Dzgradba::~CA2Dzgradba()
{
	delete random;
	delete [] t0;
	delete [] t1;
}

void CA2Dzgradba::IniciacijaCelic()
{
	int x,y,ko;
	int barva = 0;


		for (y = 0; y < Nny; y++) {
			for (x = 0; x < Nnx; x++) {
				ko = x + Nnx*y;
					t0[ko].barva = barva;
			}
		}

}

void CA2Dzgradba::RobniPogoj()
{
	RobniPogojLp();
	RobniPogojDp();
	RobniPogojZp();
	RobniPogojSp();

	t0[0] = t0[Nx+Nnx*Ny];
	t0[(Nx+1)+Nnx*(Ny+1)] = t0[1+Nnx];
	t0[Nx+1] = t0[1+Nnx*Ny];
	t0[Nnx*(Ny+1)] = t0[Nx+Nnx];
}

void CA2Dzgradba::RobniPogojLp()
{
    for (int y = 1; y <= Ny; y++) {
        t0[Nnx*y] = t0[Nx+Nnx*y];
    }
}

void CA2Dzgradba::RobniPogojDp()
{
    for (int y = 1; y <= Ny; y++) {
        t0[Nx+1+Nnx*y] = t0[Nx+Nnx*y];
    }
}

void CA2Dzgradba::RobniPogojZp()
{
    for (int x = 1; x <= Nx; x++) {
       t0[x] = t0[x+Nnx*Ny];
    }
}

void CA2Dzgradba::RobniPogojSp()
{
    for (int x = 1; x <= Nx; x++) {
        t0[x+Nnx*(Ny+1)] = t0[x+Nnx];
    }
}

void CA2Dzgradba::Zamenjaj()
{
	Celica *pc;
	pc = t0;
	t0 = t1;
	t1 = pc;
}



void CA2Dzgradba::PrimarnaNukleacija(int stevilo_nukleusov)  // nasičena nukleacija
{
	int koorx, koory, ko;

	for (int i = 0;i<stevilo_nukleusov;i++) {
		koorx = 1+(int)((float)(Nx-1)*random->NewRand());
		koory = 1+(int)((float)(Ny-1)*random->NewRand());
		ko = koorx + Nnx*koory;
		if (t0[ko].barva != 0)
			i--;
		else
			t0[ko].barva = i+1;
	}

}

int CA2Dzgradba::HitrostNukleacije(int  stevilo_nukleusov, int prejsnja_barva)  // če je predpisana hitrost nukleacije
{

	int koorx, koory, ko, usp = 0;

	if(stevilo_nukleusov<1)
		stevilo_nukleusov = 1;

	for (int i = 0;i<stevilo_nukleusov;i++) {
		koorx = 1+(int)((float)(Nx-1)*random->NewRand());
		koory = 1+(int)((float)(Ny-1)*random->NewRand());
		ko = koorx + Nnx*koory;
		if (t0[ko].barva == 0) {
			usp++;
			t0[ko].barva = prejsnja_barva+usp;
		}
	}
	return prejsnja_barva+usp;
}

Celica CA2Dzgradba::Celica2D(int i, int j,int katera)
{
	Celica *pc;
	if (katera == 0)
		pc = t0;
	else
		pc = t1;

	return pc[i + Nnx*j];
}

int CA2Dzgradba::NormalnaRastVN(int i, int j)  // funkcija za rast posamezne celice
{
	int vracilo = 0;
	float p1;
	int kVN[5];

	kVN[0] = i + Nnx*j;
	kVN[1] = i-1 + Nnx*j;
	kVN[2] = i+1 + Nnx*j;
	kVN[3] = i + Nnx*(j-1);
	kVN[4] = i + Nnx*(j+1);

	if (t0[kVN[0]].barva == 0) {
		int prob = 0;
		int barva = 0;
		int got = 0;
		for (int s = 1;s<5;s++) {
			if (t0[kVN[s]].barva>0) {
				prob = prob+1;
					barva = t0[kVN[s]].barva;
			}
		}
		p1 = ((float)prob)/4;

		if (p1 >= random->NewRand()) {
			t1[kVN[0]].barva = barva;
		}
		else
			t1[kVN[0]].barva = t0[kVN[0]].barva;

	vracilo = 1;
	}
	else {
		t1[kVN[0]].barva = t0[kVN[0]].barva;
	}
	return vracilo;
}

/*
 * "MicroZapis":
 * Zapiše mikrostrukturo v datoteko,
 * funkcija prejme zaporedno številoko datoteke
 */
void CA2Dzgradba::MicroZapis(int gstev)
{
    int bar, i,j;
    char imef[30];
    sprintf(imef,"../myout%04d.txt",gstev);
    
    FILE *out1 = fopen(imef,"w+");

	for (j=0;j<Ny;j++) {
			for (i=0;i<Nx;i++) {
				bar = Celica2D(i+1,j+1,0).barva;
				while (bar>255) {  // za prikazovanje z 255 barvami
				      bar-=255;
                } 
				fprintf(out1,"%d %d %d\n",j,i,bar);
			}
    }
	fclose(out1);
}

/*
 * "PorazdelitevPoVelikosti":
 * Funkcija prejme stevilko datoteke v katero zapiše
 * porazdelitev po velikosti in vrne povprečno velikost zrn
 */
float CA2Dzgradba::PorazdelitevPoVelikosti(int steviloZrn)
{
      float stbin=20.0;  // stevilo razredov v porazdelitvi
      int stbini=(int)stbin;
      int i,j,k=0;
      float pvz;
      FILE *outdat = fopen("../poraz.csv","w+");
      float max=0,min=sqrt((float)(Nx*Ny));

      vector<int> zrno(steviloZrn,0);
//printf("OK %d\n",zrno.size());
      for (j=0;j<Ny;j++) {
			for (i=0;i<Nx;i++) {
                zrno[Celica2D(i+1,j+1,1).barva-1]+=1;
           }
      }
      vector <float> dp;
      float tdp;
      for (i=0;i<steviloZrn;i++)
      {
          if (zrno[i]!=0) {
//              fprintf(outdat,"%d\n",zrno[i]);
              k++;
              tdp=sqrt((float)zrno[i]);
              dp.push_back(tdp);
              if(tdp<min)
                min=tdp;
              if(tdp>max)
                max=tdp;
          }          
      }
//      fprintf(outdat,"\n%d\n",k);
      pvz=sqrt(float(Nx*Ny)/float(k));  // Povprečna velikost zrn
 //     printf("povprecna velikost zrn: %g  %g\n",min,max);

      float dbin=(max-min)/stbin;
      vector <int> bin(stbini);
      for(i=0;i<dp.size();i++) {
         int ig = (int) ((dp[i]-min) / dbin);
         if(ig>(stbini-1))
            ig=stbini-1;
         bin[ig]+=1;
      }
      for (i=0;i<stbini;i++) {
     //     fprintf(outdat,"%g,%g,%g,%d\n",min+dbin*(i+0.5),(min+dbin*(i+0.5))/pvz, log((min+dbin*(i+0.5))/pvz),bin[i]);
         fprintf(outdat,"%g,%g,%g,%d\n",min+dbin*(i+0.5),(min+dbin*(i+0.5))/pvz, ((float)bin[i])/dp.size(),bin[i]);
      }
      
	  fclose(outdat);
	  
	  return  pvz;
}
