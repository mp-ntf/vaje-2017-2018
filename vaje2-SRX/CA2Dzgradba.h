#ifndef CA2Dzgradba_H
#define CA2Dzgradba_H

#include <math.h>
#include "Rand.h"
#include <stdio.h>
#include <vector>
//#include "Zrno.h"

using namespace std;

class Celica  
{
public:
	int barva;
};

class CA2Dzgradba  
{
public:
	CA2Dzgradba(int, int);
	virtual ~CA2Dzgradba();
	int HitrostNukleacije(int, int);
	void PrimarnaNukleacija(int);
	void RobniPogoj();
	void Zamenjaj();
	int NormalnaRastVN(int,int);
	Celica Celica2D(int, int, int);
		/**
		 * "MicroZapis":
		 * Zapiše mikrostrukturo v datoteko,
		 * funkcija prejme zaporedno številoko datoteke
		 */
	void MicroZapis(int gstev);
		/**
		 * "PorazdelitevPoVelikosti":
		 * Funkcija prejme stevilo zrn, v datoteke pa zapiše
		 * porazdelitev po velikosti in vrne povprečno velikost zrn
		 */
	float PorazdelitevPoVelikosti(int steviloZrn);
private:
	void RobniPogojLp();
	void RobniPogojDp();
	void RobniPogojZp();
	void RobniPogojSp();
	void IniciacijaCelic();
	Rand *random;
	int Nx;
	int Ny;
	int Nnx;
	int Nny;
	float vc;
	Celica *t0;
	Celica *t1;
	bool abnor;
};

#endif