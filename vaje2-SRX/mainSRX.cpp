#include <stdio.h>
#include "CA2Dzgradba.h"
#define DIMENZIJAX 300
#define DIMENZIJAY 300


int main()
{
		CA2Dzgradba zg(DIMENZIJAX,DIMENZIJAY);

	int stevilo_nukleusov=20;  // število nukleusov za nasičeno nukleacijo, drugače ==0
	int hitrost_nukleusov=10;    // hitrost nukleacije pri zvezni nukleaciji

	FILE *avrami = fopen("../avrami.csv","w+");
	int continous = 0;
	int prbar = 0;


	if (stevilo_nukleusov == 0)   
		continous = 1;                   // zvezna (kontinuirna) nukleacija
	else {
    	zg.PrimarnaNukleacija(stevilo_nukleusov);  // nasičena nukleacija
    	prbar=stevilo_nukleusov;
     }

	int naprej = 1, kcont;
	int i,j;
	float frac = 0;    // za določitev deleža rekristaliziranega matetiala
	float cas_korak = 0;
	float delez_x = 0;

	kcont = 0;

	while (naprej == 1) {   // pomikanje po času 
		zg.RobniPogoj();   // periodični RP
		naprej = 0;
		frac = 0;
		cas_korak = cas_korak + 1;
		if (continous == 1) {
  //        if(!(kcont%4))
		    	prbar=  zg.HitrostNukleacije(hitrost_nukleusov,prbar);   // klic fukcije za zvezbo nukleacijo
        }
			for (j=0;j<DIMENZIJAY;j++) {                   // zanka po vseh celicah C(i,j)
				for (i=0;i<DIMENZIJAX;i++) {
					if (zg.NormalnaRastVN(i+1,j+1) == 1)
						naprej = 1;
					if (zg.Celica2D(i+1,j+1,1).barva > 0)
						frac= frac+1;
				}
			}

		delez_x = frac/(DIMENZIJAX*DIMENZIJAY);   // delež oz. stopnja rekristalizacije, X

		zg.Zamenjaj();   // zamenjamo mreži med seboj
		fprintf(avrami,"%g, %g, %g, %g\n",log(cas_korak),log(-log(1-delez_x)),cas_korak,delez_x); // izpis v datoteko
		fflush(avrami);
		
       zg.MicroZapis(kcont);   // zapis mikrostrukture v datoteke myout%.txt
       kcont++;
	}
	
	printf("D=%g\n",zg.PorazdelitevPoVelikosti(prbar));   // izpis porazdelitve v datoteko in izpis povprečne velikosti zrn
    zg.MicroZapis(kcont);

	fclose(avrami);
    printf("Skupaj smo izpisali %d datotek.\n", kcont+1);
    printf("\n\7");

	return 0;
}
