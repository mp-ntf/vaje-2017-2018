##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=vaje2-SRX
ConfigurationName      :=Debug
WorkspacePath          := "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum"
ProjectPath            := "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje2-SRX"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=
Date                   :=06/03/19
CodeLitePath           :="/home/matjaz/.codelite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="vaje2-SRX.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/mainSRX.cpp$(ObjectSuffix) $(IntermediateDirectory)/Rand.cpp$(ObjectSuffix) $(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/mainSRX.cpp$(ObjectSuffix): mainSRX.cpp $(IntermediateDirectory)/mainSRX.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje2-SRX/mainSRX.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/mainSRX.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/mainSRX.cpp$(DependSuffix): mainSRX.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/mainSRX.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/mainSRX.cpp$(DependSuffix) -MM "mainSRX.cpp"

$(IntermediateDirectory)/mainSRX.cpp$(PreprocessSuffix): mainSRX.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/mainSRX.cpp$(PreprocessSuffix) "mainSRX.cpp"

$(IntermediateDirectory)/Rand.cpp$(ObjectSuffix): Rand.cpp $(IntermediateDirectory)/Rand.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje2-SRX/Rand.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Rand.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Rand.cpp$(DependSuffix): Rand.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Rand.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Rand.cpp$(DependSuffix) -MM "Rand.cpp"

$(IntermediateDirectory)/Rand.cpp$(PreprocessSuffix): Rand.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Rand.cpp$(PreprocessSuffix) "Rand.cpp"

$(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix): CA2Dzgradba.cpp $(IntermediateDirectory)/CA2Dzgradba.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje2-SRX/CA2Dzgradba.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/CA2Dzgradba.cpp$(DependSuffix): CA2Dzgradba.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/CA2Dzgradba.cpp$(DependSuffix) -MM "CA2Dzgradba.cpp"

$(IntermediateDirectory)/CA2Dzgradba.cpp$(PreprocessSuffix): CA2Dzgradba.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/CA2Dzgradba.cpp$(PreprocessSuffix) "CA2Dzgradba.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


