//============================================================================
// Name        : PrecFeCuNucl.cpp
// Author      : Goran Kugler
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <math.h>
#include <stdio.h>
#define STRAZ 500     // število razredov

#define PI 3.14159265
#define R 8.314472     // splošna plinska konstanta J/K mol

using namespace std;

class KErazred
{
public:
	double N;     // število izločkov na enoto volumna
	double r;    // radij (i)
};

double zacetniPogoj(vector<KErazred> &razred)
{
	double r0=0.45e-9;
	double rmax=4.7e-9;
	double dr=(rmax-r0)/STRAZ;
	double r=r0;
	double sg=0.2;
	double rmean=log(1.5e-9)-sg*sg/2;
	KErazred rzr;
	double N=0.0;
	for(int i=0;i<STRAZ;i++)
	{
		rzr.r=r;
		rzr.N=dr*exp(-(log(r)-rmean)*(log(r)-rmean)/(2*sg*sg))/(r*sg*sqrt(2*PI));
		razred.push_back(rzr);
		N=N+razred[i].N;
		r=r+dr;
	}
	return N;
}

double hitrost(double D,double x,double xe, double r)
{
	double v=D*(x-xe)/(r*(1-xe));

	return v;
}
double Xe(double xe0, double gama,double vat,double r, double T)
{
	double kB=1.3806504e-23;
	double bxe=xe0*exp(2*gama*vat/(r*kB*T));

	return bxe;
}

void reScale(vector<KErazred> &razred,double fT,double fTp)
{
	double ceta = fTp/fT;

	for(int i=0;i<razred.size();i++)
	{
		razred[i].N=razred[i].N*ceta;
	}

}

void fTrmean(vector<KErazred> &razred, double *fT,double *rmean, double *nN)
{
	double r;
	double N=0;

	for(int i=0;i<razred.size();i++)
	{
		r=razred[i].r;
		N=N+razred[i].N;
		(*rmean)=(*rmean)+razred[i].N*r;
		(*fT)=(*fT)+razred[i].N*r*r*r;
//		fprintf(out,"%E,%E\n",razred[i].r,razred[i].N);
	}
	(*fT)=(*fT)*4*PI/3;
	(*rmean)=(*rmean)/N;
	(*nN)=N;
}

double inserRaz(vector<KErazred> &razred, double rmax)
{
	double fT=0;
	double r;
	KErazred rzr;

	for(int i=0;i<(razred.size()-2);i++)
	{
		if(fabs(razred[i+1].r-razred[i].r)>rmax)
		{
			rzr.r=0.5*(razred[i+1].r+razred[i].r);
			rzr.N=0.25*(razred[i].N+razred[i+1].N*(razred[i+1].r-razred[i].r)/(razred[i+2].r-razred[i+1].r));
			razred[i].N=0.5*razred[i].N;
			razred.insert(razred.begin()+i+1,rzr);
		}
		r=razred[i].r;
		fT=fT+razred[i].N*r*r*r;
	}

	r=razred[razred.size()-2].r;
	fT=fT+razred[razred.size()-2].N*r*r*r;
	r=razred[razred.size()-1].r;
	fT=fT+razred[razred.size()-1].N*r*r*r;

	fT=fT*4*PI/3;

	return fT;
}


int nuclKor(vector<KErazred> &razred, double dt,double D,double x, double rcrt,double vat,double gama,double T,double Nmin,double t)
{
	KErazred rzr;
	double a=3.515e-10;
	double kB=1.3806504e-23;
	double N;
	double z;
	double betaN0;
	double dG=4*PI*rcrt*rcrt*gama/3;
	double betaz2;
	int nuki=0;

	betaN0=4*PI*rcrt*rcrt*D*x/pow(a,7);
	z=vat*sqrt(gama/(kB*T))/(2*PI*rcrt*rcrt);
	betaz2=z*z*4*PI*rcrt*rcrt*D*x/pow(a,4);
	double taut=2*betaz2*t;
	double incCas=1-exp(-taut);

	N=betaN0*z*exp(-dG/(kB*T))*incCas*dt;

	if (N>(Nmin*1.0e-10)) {
		rzr.N=N;
		rzr.r=1.05*rcrt;
		razred.push_back(rzr);
		nuki=1;
	}
//	printf("N=%E\n",N);
	return nuki;
}

void izpisPor(vector<KErazred> &razred,char *imedat)
{
	FILE *out;

	out=fopen(imedat,"w+");

	for(int i=0;i<(razred.size()-1);i++)
	{
		fprintf(out,"%E,%E,%E\n",razred[i].r,razred[i].N,razred[i].N/(razred[i].r-razred[i+1].r));  // izpise r,Ni,Di
	}

	fclose(out);
}

void odvisnoT(double T,double d0, double Q,double *dif, double *xe0v)
{
	(*dif)=d0*exp(-Q/(R*T));
	double xe0=pow(10.0,5771323/(T*T)-15763.84/T+9.944961);
	(*xe0v)=xe0*55.845/(100*63.546);
}


int main()
{
	KErazred rzr;   //trenutni razred
	double dt=1.0e-3; // casovni korak
	double t=dt;
	double tk=1.0e7;
	double gama=0.15;
	double x0=0.0123;      //Začetna koncentracija
	double D0=6.0e-8;
	double Q=166400;
	double vat=1.182e-29;
	double kB=1.3806504e-23;
	double x;
	double T=973;     //Temperatura
	double fTp=0.01;
	int i;
	double r;
	double N=0.0;
	double dr;
	FILE *out=fopen("out.csv","w+");

	std::vector<KErazred> razred;


	double fT=0.0;
	double rmean=0.0;
	double rcrt;

	x=x0;

	double xe;
	double D=D0*exp(-Q/(R*T));
	double Tc=T;
	double xe0=pow(10.0,5771323/(Tc*Tc)-15763.84/Tc+9.944961);
	xe0=xe0*55.845/(100*63.546);
	double vh;

	D=1.0*D;

	int j=0;

	rcrt=2*gama*vat/(kB*T*log(x/xe0));
	printf("x=%E xe0=%E rc=%E\n",x,xe0,rcrt);

	double rcrt1=rcrt;
	double dr_max=0;
	double dr_maxt=0;
	double rsmax=0;
	double ddrmax;
	int nuki=0;
	bool drbool = true;



//	dr_max=2*(razred[razred.size()-1].r-razred[0].r)/STRAZ;


	while(t<tk)
	{
		nuki=nuki+nuclKor(razred,dt,D,x,rcrt,vat,gama,T,N,t);
		fT=0;
		N=0.0;
		rmean=0.0;
		rsmax=0.0;
		dr_maxt=0.0;
		for(i=0;i<razred.size();i++)
		{
			if (i<(razred.size()-1)) {
				ddrmax=razred[i].r-razred[i+1].r;
				if(ddrmax>dr_maxt)
					dr_maxt=ddrmax;
			}

			xe=Xe(xe0,gama,vat,razred[i].r,T);
			vh=hitrost(D,x,xe,razred[i].r);
			dr=vh*dt;
			r=razred[i].r+dr;
			if(dr>rsmax)
			{
				rsmax=dr;
			}
//			if(vh<0)
//				printf("Raztop, %E %d\n",t,i);
			razred[i].r=r;
			if(r<1.0e-10) {
				razred.erase(razred.begin()+i);
				i--;
	//			printf("Brise, %d\n",i);
			}
			else {
				fT=fT+razred[i].N*r*r*r;
				N=N+razred[i].N;
				rmean=rmean+razred[i].N*razred[i].r;
			}
		}
		fT=fT*4*PI/3;
		x=(x0-fT)/(1-fT);
		rmean=rmean/N;
		rcrt=2*gama*vat/(kB*T*log(x/xe0));

	//	printf("rsmax=%E\n",rsmax);
		////////// OKNO //////////////////
		if ((rsmax>8.0e-10)||(dt>5e2))
			dt=0.5*dt;
		if (rsmax<6.0e-10)
			dt=1.002*dt;
		////////// KONEC OKNA //////////////

		if(!(j%1)) {
//			printf("t=%.2E  rc=%.2E  fT=%.2E  x=%.2E, sz=%d\n",t,rcrt,fT,x,razred.size());
			fprintf(out,"%E,%E,%E,%E,%E,%E,%E,%E,%d,%d\n",t,fT,rcrt,x,rmean,N,dt,xe0,razred.size(),nuki);
		}
		t=t+dt;
		j++;
		fTp=fT;
		if (((nuki-razred.size())>1)&&drbool) {
//			dr_max=-2*(razred[razred.size()-1].r-razred[0].r)/((double)razred.size());
			dr_max=1.1*dr_maxt;
			printf("drmax=%E, t=%E; rmax=%E, rzadnji=%E, st=%d\n",dr_max,t,razred[0].r,razred[razred.size()-1].r,razred.size());
			drbool = false;
			izpisPor(razred,"Por1.csv");
		}
		if (!drbool)
		{
			fT=inserRaz(razred,dr_max);
			reScale(razred,fT,fTp);
		}
/*		if ((t>46)&&(t<46.1753)) {
			izpisPor(razred,"PorA.csv");
			printf("Izpis porazdelitve A, t=%g\n",t);
		}

		if ((t>505)&&(t<506.81)) {
			izpisPor(razred,"PorB.csv");
			printf("Izpis porazdelitve B, t=%g\n",t);
		}

		if ((t>8395)&&(t<8395.1)) {
			izpisPor(razred,"PorC.csv");
			printf("Izpis porazdelitve C, t=%g\n",t);
		}*/
	}   // Konec zanke

	izpisPor(razred,"Por.csv");
	printf("KONEC\n");

	fclose(out);

	return 1;
}
