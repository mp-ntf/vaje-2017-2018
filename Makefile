.PHONY: clean All

All:
	@echo "----------Building project:[ vaje3-GG - Debug ]----------"
	@cd "vaje3-GG" && "$(MAKE)" -f  "vaje3-GG.mk"
clean:
	@echo "----------Cleaning project:[ vaje3-GG - Debug ]----------"
	@cd "vaje3-GG" && "$(MAKE)" -f  "vaje3-GG.mk" clean
