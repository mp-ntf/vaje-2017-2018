//============================================================================
// Name        : PrecFeCu.cpp
// Author      : Goran Kugler
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <math.h>
#include <stdio.h>
#define STRAZ 500     // število razredov

#define PI 3.14159265
#define R 8.314472     // splošna plinska konstanta J/K mol

using namespace std;

class KErazred
{
public:
	double N;     // število izločkov na enoto volumna
	double r;    // radij (i)
};

double zacetniPogoj(vector<KErazred> &razred)   // začetna porazdelitev po velikosti
{
	double r0=0.45e-9;
	double rmax=4.7e-9;
	double dr=(rmax-r0)/STRAZ;
	double r=r0;
	double sg=0.2;
	double rmean=log(1.5e-9)-sg*sg/2;
	KErazred rzr;
	double N=0.0;
	for(int i=0;i<STRAZ;i++)
	{
		rzr.r=r;
		rzr.N=dr*exp(-(log(r)-rmean)*(log(r)-rmean)/(2*sg*sg))/(r*sg*sqrt(2*PI));
		razred.push_back(rzr);
		N=N+razred[i].N;
		r=r+dr;
	}
	return N;
}

double hitrost(double D,double x,double xe, double r)  // hitrost rasti enačba 1.7 Navodila za vajo
{
	double v=D*(x-xe)/(r*(1-xe));

	return v;
}
double Xe(double xe0, double gama,double vat,double r, double T)  // ravnovesje na meji; Gibbs-Thomson
{
	double kB=1.3806504e-23;
	double bxe=xe0*exp(2*gama*vat/(r*kB*T));

	return bxe;
}

void reScale(vector<KErazred> &razred,double fT,double fTp)
{
	double ceta = fTp/fT;

	for(int i=0;i<razred.size();i++)
	{
		razred[i].N=razred[i].N*ceta;
	}

}

void fTrmean(vector<KErazred> &razred, double *fT,double *rmean, double *nN)
{
	double r;
	double N=0;

	for(int i=0;i<razred.size();i++)
	{
		r=razred[i].r;
		N=N+razred[i].N;
		(*rmean)=(*rmean)+razred[i].N*r;
		(*fT)=(*fT)+razred[i].N*r*r*r;
//		fprintf(out,"%E,%E\n",razred[i].r,razred[i].N);
	}
	(*fT)=(*fT)*4*PI/3;
	(*rmean)=(*rmean)/N;
	(*nN)=N;
}

double inserRaz(vector<KErazred> &razred, double rmax)
{
	double fT=0;
	double r;
	KErazred rzr;

	for(int i=0;i<(razred.size()-2);i++)
	{
		if(fabs(razred[i+1].r-razred[i].r)>rmax)
		{
			rzr.r=0.5*(razred[i+1].r+razred[i].r);
			rzr.N=0.25*(razred[i].N+razred[i+1].N*(razred[i+1].r-razred[i].r)/(razred[i+2].r-razred[i+1].r));
			razred[i].N=0.5*razred[i].N;
			razred.insert(razred.begin()+i+1,rzr);
		}
		r=razred[i].r;
		fT=fT+razred[i].N*r*r*r;
	}

	r=razred[razred.size()-2].r;
	fT=fT+razred[razred.size()-2].N*r*r*r;
	r=razred[razred.size()-1].r;
	fT=fT+razred[razred.size()-1].N*r*r*r;

	fT=fT*4*PI/3;

	return fT;
}


void izpisPorazdelitve(vector<KErazred> &razred,char *imedat)
{
	FILE *out;

	out=fopen(imedat,"w+");

        printf("%s\n",imedat);

	int straz=razred.size();

	if (straz!=0) {
		for(int i=0;i<(straz-1);i++)
		{
			fprintf(out,"%E,%E,%E\n",razred[i].r,razred[i].N,razred[i].N/(razred[i+1].r-razred[i].r));  // izpise r,Ni,Di
		}
	}

	fclose(out);
}

int main()
{

	KErazred rzr;   //trenutni razred
	double dt=1.0e-3; // casovni korak
	double t=0.0;
	double tk=1.0e6;
	double gama=0.2;
	double x0=0.0123;
	double D0=6.0e-8;
	double Q=166400;
	double vat=1.182e-29;
	double kB=1.3806504e-23;
	double x;
	double T=973;  // T je v Kelvinih
	double fTp=0.01;
	int i;
	double r;
	FILE *out=fopen("out.csv","w+");

	vector<KErazred> razred;

	double N=zacetniPogoj(razred);

	double fT=0.0;
	double rmean=0.0;
	double rcrt;

	fTrmean(razred,&fT,&rmean,&N);
	reScale(razred,fT,fTp);
	fTrmean(razred,&fT,&rmean,&N);

	x=(x0-fT)/(1-fT);

	double xe;
	double D=D0*exp(-Q/(R*T));
	double Tc=T;
	double xe0=pow(10.0,5771323/(Tc*Tc)-15763.84/Tc+9.944961);
	xe0=xe0*55.845/(100*63.546);
	double vh;
    D = 1.0*D; // difuzijski koeficient :)
	int j=0;

	rcrt=2*gama*vat/(kB*T*log(x/xe0));
	printf("N=%E  Rm=%E  fT=%E  x=%E xe0=%E rc=%E\n",N,rmean,fT,x,xe0,rcrt);

	double rcrt1=rcrt;
	double dr_max;

	dr_max=2*(razred[razred.size()-1].r-razred[0].r)/STRAZ;

        izpisPorazdelitve(razred, "PorZ.csv");


	while(t<tk)
	{
		fT=0;
		N=0.0;
		rmean=0.0;
		for(i=0;i<razred.size();i++)
		{
			xe=Xe(xe0,gama,vat,razred[i].r,T);
			vh=hitrost(D,x,xe,razred[i].r);
			r=razred[i].r+vh*dt;
			razred[i].r=r;
			if(r<1.0e-10) {
				razred.erase(razred.begin()+i);
				i--;
			}
			else {
				fT=fT+razred[i].N*r*r*r;
				N=N+razred[i].N;
				rmean=rmean+razred[i].N*razred[i].r;
			}
		}
		fT=fT*4*PI/3;
		x=(x0-fT)/(1-fT);
		rmean=rmean/N;
		rcrt=2*gama*vat/(kB*T*log(x/xe0));
		if(!(j%10)) {
			fprintf(out,"%E,%E,%E,%E,%E,%E,%d\n",t,fT,rcrt,x,rmean,N,razred.size());
		}
		if (!(j%10000)) printf("t = %g \n",t);
		t=t+dt;
		j++;
		if ((fabs(rcrt-rmean)/rmean)< 0.1) {
             if(dt<10)
                dt*=1.005;
        }
		fTp=fT;
		dr_max=2*(razred[razred.size()-1].r-razred[0].r)/STRAZ;
		fT=inserRaz(razred,dr_max);
		reScale(razred,fT,fTp);
	}
	printf("xe_0=%g\n",xe0);

	fclose(out);

	izpisPorazdelitve(razred, "Por.csv");

 //   system("PAUSE");
    return 1;
}
