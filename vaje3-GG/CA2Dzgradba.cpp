#include "CA2Dzgradba.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CA2Dzgradba::CA2Dzgradba(int tdx, int tdy)
{
	Nx = tdx;
	Ny = tdy;

	Nnx = Nx+2;
	Nny = Ny+2;
	
	long int velikostv = Nnx*Nny;
	t0 = new Celica[velikostv];
	t1 = new Celica[velikostv];
	random = new Rand;

	IniciacijaCelic();
	abnor = false;
}

CA2Dzgradba::~CA2Dzgradba()
{
	delete random;
	delete [] t0;
	delete [] t1;
	if (abnor)
		delete [] abnormal;
}

void CA2Dzgradba::IniciacijaCelic()
{
	int x,y,ko;
	int barva = 0;


		for (y = 0; y < Nny; y++) {
			for (x = 0; x < Nnx; x++) {
				ko = x + Nnx*y;
					t0[ko].barva = barva;
			}
		}

}

int CA2Dzgradba::Abnormal(float frac) 
{
    int velab=steviloZrn;
	abnormal = new float[velab]; 
	abnor=true;
	int j=0;

	for (int i=0;i<velab;i++) {
		if (frac >= random->NewRand()) {
			abnormal[i]=1;
			j++;
		}
		else
			abnormal[i]=0;
	}
	return j;
}


void CA2Dzgradba::RobniPogoj()
{
	RobniPogojLp();
	RobniPogojDp();
	RobniPogojZp();
	RobniPogojSp();

	t0[0] = t0[Nx+Nnx*Ny];
	t0[(Nx+1)+Nnx*(Ny+1)] = t0[1+Nnx];
	t0[Nx+1] = t0[1+Nnx*Ny];
	t0[Nnx*(Ny+1)] = t0[Nx+Nnx];
}

void CA2Dzgradba::RobniPogojLp()
{
    for (int y = 1; y <= Ny; y++) {
        t0[Nnx*y] = t0[Nx+Nnx*y];
    }
}

void CA2Dzgradba::RobniPogojDp()
{
    for (int y = 1; y <= Ny; y++) {
        t0[Nx+1+Nnx*y] = t0[Nx+Nnx*y];
    }
}

void CA2Dzgradba::RobniPogojZp()
{
    for (int x = 1; x <= Nx; x++) {
       t0[x] = t0[x+Nnx*Ny];
    }
}

void CA2Dzgradba::RobniPogojSp()
{
    for (int x = 1; x <= Nx; x++) {
        t0[x+Nnx*(Ny+1)] = t0[x+Nnx];
    }
}

/*
 * "Zamenjaj":
 * Zamenja mreži 
 */
void CA2Dzgradba::Zamenjaj()
{
	Celica *pc;
	pc = t0;
	t0 = t1;
	t1 = pc;
}

/*
 * "Celica2D":
 * Funkcija vrne celico c(i,j) in sicer
 * za čas t ali t+1, odvisno od spremenljivke "katera"
 */
Celica CA2Dzgradba::Celica2D(int i, int j,int katera)
{
	Celica *pc;
	if (katera == 0)
		pc = t0;
	else
		pc = t1;

	return pc[i + Nnx*j];
}

/*
 * "NormalnaRastZrn":
 * Spremeni stanje celice c(i,j) v skladu s
 * lokalnim pravilom če imamo opravka s normalno rastjo
 */
void CA2Dzgradba::NormalnaRastZrn(int i, int j,float p1)
{
	int it,vracilo = 0;
	int prob = 0;
	int barva=0;
	int kVN[9];
	vector <int> izbiraP;

	kVN[0] = i + Nnx*j;    // soseščina glej enačbo 2.4 v navodilih

	kVN[1] = i-1 + Nnx*j;
	kVN[2] = i+1 + Nnx*j;
	kVN[3] = i + Nnx*(j-1);
	kVN[4] = i + Nnx*(j+1);

	kVN[5] = i-1 + Nnx*(j-1);
	kVN[6] = i+1 + Nnx*(j-1);
	kVN[7] = i-1 + Nnx*(j+1);
	kVN[8] = i+1 + Nnx*(j+1);  // konec soseščine


	for (int s = 1;s<9;s++) {   // preveri če leži celica na meji zrna
		if (t0[kVN[s]].barva==t0[kVN[0]].barva)
			prob++;
	}

	if (prob != 8) {  // celica leži na meji
		if (t0[kVN[1]].barva==t0[kVN[2]].barva) {
			if (t0[kVN[1]].barva==t0[kVN[3]].barva) {
				vracilo=1;
				barva=t0[kVN[3]].barva;
			}
			else {
				if (t0[kVN[1]].barva==t0[kVN[4]].barva) {
					vracilo = 1;
					barva=t0[kVN[4]].barva;
				}
			}
		}
		else {
			if (t0[kVN[1]].barva==t0[kVN[4]].barva) {
				if (t0[kVN[1]].barva==t0[kVN[3]].barva) {
					vracilo = 1;
					barva=t0[kVN[3]].barva;
				}
			}
			else {
				if (t0[kVN[2]].barva==t0[kVN[3]].barva) {
					if (t0[kVN[2]].barva==t0[kVN[4]].barva) {
						vracilo=1;
						barva=t0[kVN[4]].barva;
					}
				}
			}
		}

		if (vracilo==1)
			t1[kVN[0]].barva = barva;
		else {
			if (t0[kVN[5]].barva==t0[kVN[6]].barva) {
				if (t0[kVN[5]].barva==t0[kVN[7]].barva) {
					vracilo=1;
					barva=t0[kVN[7]].barva;
				}
				else {
					if (t0[kVN[5]].barva==t0[kVN[8]].barva) {
						vracilo = 1;
						barva=t0[kVN[8]].barva;
					}
				}
			}
			else {
				if (t0[kVN[5]].barva==t0[kVN[8]].barva) {
					if (t0[kVN[5]].barva==t0[kVN[7]].barva) {
						vracilo = 1;
						barva=t0[kVN[7]].barva;
					}
				}
				else {
					if (t0[kVN[6]].barva==t0[kVN[7]].barva) {
						if (t0[kVN[6]].barva==t0[kVN[8]].barva) {
							vracilo=1;
							barva=t0[kVN[8]].barva;
						}
					}
				}
			}
		}

		if (vracilo==1)
			t1[kVN[0]].barva = barva;
		else {
/*			if (p1 >= random->NewRand()) {
				vracilo=1+((int)(8*random->NewRand()));
				if (vracilo>=9)
					vracilo=8;
				t1[kVN[0]].barva = t0[kVN[vracilo]].barva;
			}
			else
				t1[kVN[0]].barva = t0[kVN[0]].barva; */
    			if (p1 >= random->NewRand()) {
                    for (it=1;it<9;it++) {
                        if(t0[kVN[it]].barva!=t0[kVN[0]].barva)
                           izbiraP.push_back(t0[kVN[it]].barva);
                    }
                    vracilo=izbiraP.size()-1;                 
    				vracilo=(int)(0.5+vracilo*random->NewRand());
                    //printf("TU\n v=%d iz=%d",vracilo,izbiraP.size());
    				t1[kVN[0]].barva = izbiraP[vracilo];
                }
                else {
                     t1[kVN[0]].barva = t0[kVN[0]].barva; 
                }

		}
	}
	else {
		t1[kVN[0]].barva = t0[kVN[0]].barva;
	}
}

/*
 * "PretiranaRastZrn":
 * Spremeni stanje celice c(i,j) v skladu s
 * lokalnim pravilom če imamo opravka s prtirano rastjo
 */
void CA2Dzgradba::PretiranaRastZrn(int i, int j,float p1,float p2)
{
	int vracilo = 0;
	int prob = 0;
	int barva=0;
	int kVN[9];
	float probver;
	int ooth[8];
	int oothi=0;

	kVN[0] = i + Nnx*j;

	kVN[1] = i-1 + Nnx*j;
	kVN[2] = i+1 + Nnx*j;
	kVN[3] = i + Nnx*(j-1);
	kVN[4] = i + Nnx*(j+1);

	kVN[5] = i-1 + Nnx*(j-1);
	kVN[6] = i+1 + Nnx*(j-1);
	kVN[7] = i-1 + Nnx*(j+1);
	kVN[8] = i+1 + Nnx*(j+1);



	for (int s = 1;s<9;s++) {
		if (t0[kVN[s]].barva==t0[kVN[0]].barva)
			prob++;
		else {
			oothi++;
			ooth[oothi-1]=t0[kVN[s]].barva;
//			other.push_back(t0[kVN[s]].barva);
		}
	}

	if (prob != 8) {
		if (t0[kVN[1]].barva==t0[kVN[2]].barva) {
			if (t0[kVN[1]].barva==t0[kVN[3]].barva) {
				vracilo=1;
				barva=t0[kVN[3]].barva;
			}
			else {
				if (t0[kVN[1]].barva==t0[kVN[4]].barva) {
					vracilo = 1;
					barva=t0[kVN[4]].barva;
				}
			}
		}
		else {
			if (t0[kVN[1]].barva==t0[kVN[4]].barva) {
				if (t0[kVN[1]].barva==t0[kVN[3]].barva) {
					vracilo = 1;
					barva=t0[kVN[3]].barva;
				}
			}
			else {
				if (t0[kVN[2]].barva==t0[kVN[3]].barva) {
					if (t0[kVN[2]].barva==t0[kVN[4]].barva) {
						vracilo=1;
						barva=t0[kVN[4]].barva;
					}
				}
			}
		}

		if (vracilo==1)
			t1[kVN[0]].barva = barva;
		else {
			if (t0[kVN[5]].barva==t0[kVN[6]].barva) {
				if (t0[kVN[5]].barva==t0[kVN[7]].barva) {
					vracilo=1;
					barva=t0[kVN[7]].barva;
				}
				else {
					if (t0[kVN[5]].barva==t0[kVN[8]].barva) {
						vracilo = 1;
						barva=t0[kVN[8]].barva;
					}
				}
			}
			else {
				if (t0[kVN[5]].barva==t0[kVN[8]].barva) {
					if (t0[kVN[5]].barva==t0[kVN[7]].barva) {
						vracilo = 1;
						barva=t0[kVN[7]].barva;
					}
				}
				else {
					if (t0[kVN[6]].barva==t0[kVN[7]].barva) {
						if (t0[kVN[6]].barva==t0[kVN[8]].barva) {
							vracilo=1;
							barva=t0[kVN[8]].barva;
						}
					}
				}
			}
		}
		if (vracilo==1)
			t1[kVN[0]].barva = barva;
		else {
			vracilo=((int)(oothi*(random->NewRand()-0.0001)));
			if (abnormal[ooth[vracilo]-1]==1)
				probver = p2;
			else
				probver = p1;
			if (probver >= random->NewRand()) {
				t1[kVN[0]].barva = ooth[vracilo];
			}
			else
				t1[kVN[0]].barva = t0[kVN[0]].barva;
		}
	}
	else {
		t1[kVN[0]].barva = t0[kVN[0]].barva;
	}
}

/*
 * "PreberiZacetnoMikrostrukturo": 
 * Prebere začetno mikrostrukturo iz datoteke 
 * z imenom "imedat". Vrne true, če je uspešno 
 * prebrano, drugače pa false. Funkcija kot
 * argument prejme ime datoteke
 */
bool CA2Dzgradba::PreberiZacetnoMikrostrukturo(char *imedat)
{
     FILE *out1;
     if ((out1 = fopen(imedat,"r"))==NULL)
          return false;     // funkcija vrne false, če ne more odpret datoteke
     
     
     int k0,i,j,barva,maxbarva=0;
     
     while(!feof(out1))  // prebere iz datoteke
     {
          fscanf(out1,"%d %d %d",&i,&j,&barva);
          k0 = i + 1 + Nnx*(j+1);
          t0[k0].barva=barva;
          if (barva>maxbarva)    // poišče število zrn v vhodni mikrostrukturi
              maxbarva=barva;
     }
     steviloZrn=maxbarva;
     printf("Stevilo zrn: %d\n",maxbarva);
     
     
     RobniPogoj(); // uveljavi periodični robni pogoj
     
     fclose(out1);
     
     return true;   // vrne true, če je vse ok!
}

/*
 * "MicroZapis":
 * Zapiše mikrostrukturo v datoteko,
 * funkcija prejme zaporedno številoko datoteke
 */
void CA2Dzgradba::MicroZapis(int gstev)
{
    int bar, i,j;
    char imef[30];
    sprintf(imef,"../myout%04d.txt",gstev);
    
    FILE *out1 = fopen(imef,"w+");

	for (j=0;j<Ny;j++) {
			for (i=0;i<Nx;i++) {
				bar = Celica2D(i+1,j+1,0).barva;
/*				while (bar>255) {  // za prikazovanje z 255 barvami
				      bar-=255;
                } */
				fprintf(out1,"%d %d %d\n",j,i,bar);
			}
    }
	fclose(out1);
}

/*
 * "PorazdelitevPoVelikosti":
 * Funkcija prejme stevilko datoteke v katero zapiše
 * porazdelitev po velikosti in vrne povprečno velikost zrn
 */
float CA2Dzgradba::PorazdelitevPoVelikosti(int stizpis)
{
      int i,j,k=0;
      float pvz;
      char imef[30];
      sprintf(imef,"../poraz%04d.csv",stizpis);
      FILE *outdat = fopen(imef,"w+");
      
      if (zrno.size()!=0)
          zrno.clear();
      zrno.resize(steviloZrn,0);
//printf("OK %d\n",zrno.size());
      for (j=0;j<Ny;j++) {
			for (i=0;i<Nx;i++) {
                zrno[Celica2D(i+1,j+1,1).barva-1]+=1;
           }
      }
      for (i=0;i<steviloZrn;i++)
      {
          if (zrno[i]!=0) {
              fprintf(outdat,"%d\n",zrno[i]);
              k++;
          }          
      }
      fprintf(outdat,"\n%d\n",k);
      pvz=float(Nx*Ny)/float(k);
//      printf("povprecna velikost zrn: %g\n",pvz);
	  fclose(outdat);
	  
	  return  pvz;
}
