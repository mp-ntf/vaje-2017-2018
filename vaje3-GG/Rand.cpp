#include "Rand.h"
#include <time.h>
#include <stdio.h>


Rand::Rand()
{
   time_t curtime;      // Set random seed number 
   time(&curtime);      // Get current time in fSeed.
   long fSeed = (long)curtime;
   num = -fSeed;


   ran2(num);   // Initialize random number generator


   r = ran2(num);  // stores random number in r
}


float Rand::GetRand()
 {
  return r;            // returns random number
 }


float Rand::NewRand()
{
   r = ran2(num);     // generates new random number, stores in r and then returns r
//	printf("%f\n",r);

   return r;
}




 /****************************Random Number Generator******************/




float Rand::ran2(long& idum)
{
   const long IM1=2147483563;
   const long IM2=2147483399;
   const double AM=(1.0/IM1);
   const long IMM1=(IM1-1);
   const long IA1=40014;
   const long IA2=40692;
   const long IQ1=53668;
   const long IQ2=52774;
   const long IR1=12211;
   const long IR2=3791;
   const long NTAB=32;
   const long NDIV=(1+IMM1/NTAB);
   const double EPS=1.2e-7;
   const double RNMX=(1.0-EPS);



   int j;
   long k;
   static long idum2=123456789;
   static long iy=0;
   static long iv[NTAB];
   float temp;

   if(idum <= 0)
   {
      if(-(idum) < 1)
         idum = 1;
      else
         idum = -(idum);

      idum2 = (idum);
      for(j=NTAB+7;j >= 0;j--)
      {
         k=(idum)/IQ1;
         idum=IA1*(idum-k*IQ1)-k*IR1;
         if(idum < 0)
            idum+=IM1;
         if(j < NTAB)
            iv[j] =idum;
      }

      iy = iv[0];
   }

   k = idum/IQ1;

   idum = IA1*(idum - k*IQ1) - k*IR1;
   if(idum < 0)
      idum += IM1;

   k = idum2/IQ2;
   idum2 = IA2*(idum2-k*IQ2)-k*IR2;
   if(idum2 < 0)
      idum2 += IM2;
   j=iy/NDIV;
   iy=iv[j]-idum2;
   iv[j] = idum;
   if (iy < 1)
      iy += IMM1;
   if ((temp = AM*iy) > RNMX)
      return RNMX;
   else
      return temp;
   }
