##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=vaje3-GG
ConfigurationName      :=Debug
WorkspacePath          := "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum"
ProjectPath            := "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje3-GG"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=
Date                   :=06/03/19
CodeLitePath           :="/home/matjaz/.codelite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="vaje3-GG.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/mainGG.cpp$(ObjectSuffix) $(IntermediateDirectory)/Rand.cpp$(ObjectSuffix) $(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/mainGG.cpp$(ObjectSuffix): mainGG.cpp $(IntermediateDirectory)/mainGG.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje3-GG/mainGG.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/mainGG.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/mainGG.cpp$(DependSuffix): mainGG.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/mainGG.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/mainGG.cpp$(DependSuffix) -MM "mainGG.cpp"

$(IntermediateDirectory)/mainGG.cpp$(PreprocessSuffix): mainGG.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/mainGG.cpp$(PreprocessSuffix) "mainGG.cpp"

$(IntermediateDirectory)/Rand.cpp$(ObjectSuffix): Rand.cpp $(IntermediateDirectory)/Rand.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje3-GG/Rand.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Rand.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Rand.cpp$(DependSuffix): Rand.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Rand.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/Rand.cpp$(DependSuffix) -MM "Rand.cpp"

$(IntermediateDirectory)/Rand.cpp$(PreprocessSuffix): Rand.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Rand.cpp$(PreprocessSuffix) "Rand.cpp"

$(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix): CA2Dzgradba.cpp $(IntermediateDirectory)/CA2Dzgradba.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/matjaz/doktorat/workspace/vaje-mp-workspace-new/modelski-praktikum/vaje3-GG/CA2Dzgradba.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/CA2Dzgradba.cpp$(DependSuffix): CA2Dzgradba.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/CA2Dzgradba.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/CA2Dzgradba.cpp$(DependSuffix) -MM "CA2Dzgradba.cpp"

$(IntermediateDirectory)/CA2Dzgradba.cpp$(PreprocessSuffix): CA2Dzgradba.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/CA2Dzgradba.cpp$(PreprocessSuffix) "CA2Dzgradba.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


