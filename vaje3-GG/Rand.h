#ifndef Rand_H
#define Rand_H

class Rand{

public:
// default constructor generates 1st random number and stores it in r
// chose betwen ran2 in ANSI C random generator - not yet
	Rand();
	float ran2(long&); // returns a uniform Random deviate between 0.0 and 1.0
	float NewRand(); // Generates a new random number, stores it in r and then returns r 
	float GetRand(); // returns a random number 

protected:
	long num; // seed 
	float r; // random number 
};

#endif
