#include "CA2Dzgradba.h"
#define DIMX 300
#define DIMY 300
#define KORAK 200
#define MKOR 50001

void Normalna() {       // Normalna rast zrn
    int kcont,i,j,prbar=0;
    float stizpis=float(MKOR)/float(KORAK);
    FILE *pvz=fopen("PVZ.csv","w+");

	CA2Dzgradba *zg = new CA2Dzgradba(DIMX,DIMY);
	
	if (zg->PreberiZacetnoMikrostrukturo("../Zac-myout23.txt")) {

        for (kcont=0;kcont<MKOR;kcont++) {
            zg->RobniPogoj();
            for (j=0;j<DIMY;j++) {
                for (i=0;i<DIMX;i++) {
                    zg->NormalnaRastZrn(i+1,j+1,0.01);    // verjetnost
                }
            }
            zg->Zamenjaj();
            if((kcont%KORAK)==0) {
                prbar++;
                zg->MicroZapis(prbar);
                fprintf(pvz,"%d,%g\n",kcont,zg->PorazdelitevPoVelikosti(prbar));
				fflush(pvz);
                printf("stanje:%.1f %%\n",100*prbar/stizpis);
            }
        }
    }
    else printf("Ne morem prebrati vhodne mikrostrukture. \n");
	delete zg;
	
	fclose(pvz);

	printf("\n\7");
}


void Pretirana() {  // pretirana rast zrn

	CA2Dzgradba *zg = new CA2Dzgradba(DIMX,DIMY);
	
	if (zg->PreberiZacetnoMikrostrukturo("../Zac-myout30.txt")) {

        printf("STEVILO IZBRANIH: %d\n",zg->Abnormal(0.03));
        int kcont,i,j,prbar=0;
        for (kcont=0;kcont<102001;kcont++) {
            zg->RobniPogoj();
            for (j=0;j<DIMY;j++) {
                for (i=0;i<DIMX;i++) {
    //				if (0)
                    if (kcont>1200)
                        zg->PretiranaRastZrn(i+1,j+1,0.003,0.01);
                    else
                        zg->NormalnaRastZrn(i+1,j+1,0.01); 
                }
            }
            zg->Zamenjaj();
            if((kcont%300)==0) {
                prbar++;
                zg->MicroZapis(prbar);
            }
        }
    }
       else printf("Ne morem prebrati vhodne mikrostrukture. \n");

	delete zg;

	printf("\n\7");
}

int main() {
	Normalna();  // Normalna rast zrn
//	Pretirana();   // Pretirana rast zrn

    return 0;
}
