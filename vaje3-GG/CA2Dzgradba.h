#include <math.h>
#include <stdio.h>
#include<vector>
#include "Rand.h"
 
using namespace std;

class Celica  
{
public:
	int barva;
};

class CelicaKoor  
{
public:
	int x;
	int y;
	int z;
};

class CA2Dzgradba  
{
public:
	    CA2Dzgradba(int, int);  // konstruktor
	    virtual ~CA2Dzgradba(); // destruktor
        /**
         * "Zamenjaj":
         * Zamenja mreži 
         */	    
        void RobniPogoj();
        /**
         * "Zamenjaj":
         * Zamenja mreži 
         */	    
        void Zamenjaj();
        /**
         * "Celica2D":
         * Funkcija vrne celico c(i,j) in sicer
         * za čas t ali t+1, odvisno od spremenljivke "katera"
         */	    
        Celica Celica2D(int, int, int);
        /**
         * "NormalnaRastZrn":
         * Spremeni stanje celice c(i,j) v skladu s
         * lokalnim pravilom če imamo opravka s normalno rastjo
         */        
        void NormalnaRastZrn(int, int,float);
        /**
         * "PretiranaRastZrn":
         * Spremeni stanje celice c(i,j) v skladu s
         * lokalnim pravilom če imamo opravka s prtirano rastjo
         */
 	    void PretiranaRastZrn(int, int,float ,float);
        int Abnormal(float); 
		/**
		 * "PreberiZacetnoMikrostrukturo": 
         * prebere začetno mikrostrukturo iz datoteke 
		 * z imenom "imedat". Vrne true, če je uspešno 
		 * prebrano, drugače pa false. Funkcija kot
		 * argument prejme ime datoteke
		 */
		bool PreberiZacetnoMikrostrukturo(char *);
		/**
		 * "MicroZapis":
		 * Zapiše mikrostrukturo v datoteko,
		 * funkcija prejme zaporedno številoko datoteke
		 */
		void MicroZapis(int gstev);
		/**
		 * "PorazdelitevPoVelikosti":
		 * Funkcija prejme stevilko datoteke v katero zapiše
		 * porazdelitev po velikosti in vrne povprečno velikost zrn
		 */
		float PorazdelitevPoVelikosti(int stizpis);
private:
	void RobniPogojLp();
	void RobniPogojDp();
	void RobniPogojZp();
	void RobniPogojSp();
	void IniciacijaCelic();
	Rand *random;
	int Nx;
	int Ny;
	int Nnx;
	int Nny;
	int steviloZrn;
	float vc;
	Celica *t0;
	Celica *t1;
	float *abnormal;
	bool abnor;
	vector <int> zrno;
};
